import sys

from twisted.internet import reactor, stdio
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.protocols.basic import LineReceiver

# from rip import RIPStack
import lab2stack
from playground.twisted.endpoints import GateClientEndpoint
import getopt

global hoststr, echoprotocol, port
host = 'localhost'
port = 8124
hoststr = host + ':' + str(port)
echoprotocol = object


class EchoProtocol(Protocol):
    global hoststr
    flag = True

    def connectionMade(self):
        sys.stdout.write('>>>')
        sys.stdout.flush()
        self.log(hoststr, 'con')

    def sendMessage(self, data):
        self.log(hoststr, 'snd', data)
        self.transport.write(data)

    def dataReceived(self, data):
        self.log(hoststr, 'rcv', data)
        sys.stdout.write(data)
        sys.stdout.write('\n>>>')
        sys.stdout.flush()

    def log(self, server, type, data = ''):
        serveraddr = '[' + server + ']'
        if type == 'con':
            logstr = '[CON]' + 'connected to ' + serveraddr
        elif type == 'rcv':
            logstr = '[RCV]' + serveraddr + '\n' + data
        elif type == 'snd':
            logstr = '[SND]' + serveraddr + '\n' + data
        elif type == 'req':
            logstr = '[REQ]' + serveraddr + data
        # print logstr


class EchoFactory(ClientFactory):
    global host, port, echoprotocol

    def startedConnecting(self, connector):
        print 'Connecting to ' + hoststr

    def buildProtocol(self, addr):
        return echoprotocol

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection ', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection Failed ', reason

class StdIO(LineReceiver):
    global echoprotocol
    delimiter = '\n'

    # def connectionMade(self):
    #     self.transport.write('>>>')

    def lineReceived(self, line):
        if line == 'q':
            echoprotocol.transport.loseConnection()
            return
        echoprotocol.transport and echoprotocol.sendMessage(line)

def main(argv):
    global echoprotocol, port
    usage = "python echoclient.py [-s echo_server_address] [-p echo_server_port]"
    serveraddr = "20164.1.47806.1"
    serverport = port
    try:
        opts, args = getopt.getopt(argv[1:], 's:p:')
    except getopt.GetoptError, err:
        print str(err)
        print usage
        sys.exit(2)

    for o, a in opts:
        if o == '-s':
            serveraddr = a
        elif o == '-p':
            serverport = int(a)

    echoprotocol = EchoProtocol()
    #d = reactor.connectTCP(host, port, EchoFactory())
    stdio.StandardIO(StdIO())

# endpoint=GateClientEndpoint.CreateFromConfig(reactor,"20164.0.0.1", port,"xhyumiracle", networkStack=lab2stack)
# endpoint = GateServerEndpoint(reactor, port, "127.0.0.1", 19090, networkStack=lab2stack)
    endpoint=GateClientEndpoint(reactor, serveraddr, serverport, '127.0.0.1', 19090, networkStack=lab2stack)
    endpoint.connect(EchoFactory())
    reactor.run()

if __name__ == '__main__':
    main(sys.argv)
