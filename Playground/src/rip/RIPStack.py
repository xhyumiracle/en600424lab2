'''
Created on Oct 18, 2016

@author: xhyu
'''
from twisted.internet.protocol import Protocol, Factory
from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin
# from RIPClientProtocol import RipClientProtocol
# from RIPServerProtocol import RipServerProtocol
from RIPClientProtocolSimple import RipClientProtocol
from RIPServerProtocolSimple import RipServerProtocol

class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol


class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

# TODO: no sign in handshake

ConnectFactory = RipClientFactory
ListenFactory = RipServerFactory