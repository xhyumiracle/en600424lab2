import CertFactory
from RIPConfig import logger, signal, TIMEOUT_CLOSE
from RIPCrypto import RipCrypto
from RIPMessage import RipMessage
from RIPTransportNew import RipTransport
from playground.network.common.Protocol import StackingProtocolMixin
from playground.network.common.Timer import OneshotTimer
from twisted.internet.error import  ConnectionDone
from twisted.internet.protocol import Protocol


class RipBaseProtocol(StackingProtocolMixin, Protocol):
    sm = None
    higherTransport = None
    recBuffer = ""
    nonce = 0
    peerNonce = 0
    certs = []
    peerCert = ""
    sessionId = ""
    peerSessionId = ""
    seqNum = 0
    ackNum = 0
    lastAckNum = 0
    ACK = False
    crypto = None
    mySkBytes = ""
    timerFin = None
    allAckRcv = True
    rootCert = ""
    def __init__(self):
        self.timerFin = OneshotTimer(self.timeoutFin)
        pass

    '****************************************************'
    ' Trivial Stuff                                      '
    '****************************************************'
    def verifyCerts(self, ripMessage, indCert):
        certs = ripMessage.certificate
        if len(certs) < indCert+2:
            logger.log('err', 'len(certs) < len(nonce) + 2')
            return False
        if not self.crypto.verifyCertChain(self.rootCert, certs[indCert + 1]):
            logger.log('err', 'mid cert chain verify failed')
            return False
        if not self.crypto.verifyCertChain(certs[indCert + 1], certs[indCert + 0]):
            logger.log('err', 'peer cert chain verify failed')
            return False
        return True

    def verifyCommonName(self, ripMessage, indCert):
        certs = ripMessage.certificate
        if self.crypto.getCommonName(certs[indCert + 0]) != self.peerAddr:
        # if self.crypto.getCommonName(certs[indCert + 0]) != '20164.1.47806.1':
            logger.log('err', "verify common name failed, form cert: "+self.crypto.getCommonName(certs[indCert + 0]) + ", peer address: " + str(self.peerAddr))
            return False
        return True

    def verifySignature(self, ripMessage):
        certBytes = self.peerCert
        signature = ripMessage.signature
        ripMessage.signature = ""
        data = ripMessage.__serialize__()
        if not self.crypto.verifySignature(certBytes, data, signature):
            logger.log('err', 'signature verify failed')
            return False
        return True

    '****************************************************'
    ' Trivial Stuff End                                  '
    '****************************************************'

    def checkAck(self, ripMessage):
        if ripMessage.acknowledgement_number != self.seqNum:
            logger.log('err', 'ack (%d)!= self.seq(%d)' %(ripMessage.acknowledgement_number, self.seqNum))
            logger.log('rip', ripMessage)
            return False
        if ripMessage.close_flag:
            self.timerFin.cancel()
            self.sm.signal(signal.rcvcls, None)
            Protocol.connectionLost(self, reason=ConnectionDone)
            self.higherProtocol().connectionLost(ConnectionDone)
            self.higherProtocol().transport=None
            self.setHigherProtocol(None)
            return False  # do not allow onEnterCloseSnt to renew timerFin
        self.higherTransport.timerStop()
        return True

    def checkPacketValid(self, ripMessage):
        if not self.verifySignature(ripMessage):
            return False
        if ripMessage.sessionID != self.peerSessionId:
            logger.log('err', 'verify sessionID failed')
            return False
        return True

    def sendAck(self, ackNum):
        ripMessage = RipMessage()
        ripMessage.sequence_number = 0
        ripMessage.acknowledgement_number = ackNum
        ripMessage.certificate = []
        ripMessage.sessionID = self.sessionId
        ripMessage.acknowledgement_flag = True
        ripMessage.close_flag = False
        ripMessage.sequence_number_notification_flag = False
        ripMessage.reset_flag = False
        ripMessage.data = ""
        ripMessage.OPTIONS = []
        ripMessage.signature = ""

        signature_data = ripMessage.__serialize__()
        signature = self.crypto.sign(signature_data)
        ripMessage.signature = signature

        self.higherTransport.writeRipMessage(ripMessage)

    def sendCloseAndReset(self, close=True, reset=False, ACK=False):
        ripMessage = RipMessage()
        ripMessage.sequence_number = self.seqNum
        ripMessage.acknowledgement_number = self.ackNum
        ripMessage.certificate = []
        ripMessage.sessionID = self.sessionId
        ripMessage.acknowledgement_flag = ACK
        ripMessage.close_flag = close
        ripMessage.sequence_number_notification_flag = False
        ripMessage.reset_flag = reset
        ripMessage.data = ""
        ripMessage.OPTIONS = []
        ripMessage.signature = ""

        signature_data = ripMessage.__serialize__()
        signature = self.crypto.sign(signature_data)
        ripMessage.signature = signature

        self.higherTransport.writeRipMessage(ripMessage)
        self.lastSeqNum = self.seqNum
        self.seqNum += 1

    def processPacketContent(self, ripMessage):
        # not check sequence number before close flag
        if ripMessage.close_flag:
            self.lastAckNum = self.ackNum
            self.ackNum = ripMessage.sequence_number + 1
            self.sm.signal(signal.rcvcls, None)
            return
        if ripMessage.sequence_number == self.ackNum:
            self.lastAckNum = self.ackNum
            self.ackNum = ripMessage.sequence_number + len(ripMessage.data)
            self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
            self.sendAck(self.ackNum)
        elif ripMessage.sequence_number == self.lastAckNum:
            self.sendAck(self.ackNum)

    def receivePacket(self, ripMessage):
        if self.checkPacketValid(ripMessage):
            if ripMessage.acknowledgement_flag:
                return self.checkAck(ripMessage)
            else:
                self.processPacketContent(ripMessage)
                return True
        else:
            return False

    def connectionMade(self):
        self.myAddr = str(self.transport.getHost().host)
        self.peerAddr = str(self.transport.getPeer().host)
        logger.log('info', 'my address:' + self.myAddr)
        logger.setPeer(self.peerAddr)
        self.mySkBytes = CertFactory.getPrivateKeyForAddr(self.myAddr)
        self.certs = CertFactory.getCertsForAddr(self.myAddr)
        self.rootCert = CertFactory.getRootCert()
        self.crypto = RipCrypto(self.mySkBytes)
        self.higherTransport = RipTransport(self.transport, self)

    def closeAndTimer(self):
        self.sendCloseAndReset(True, False)
        self.timerFin.run(TIMEOUT_CLOSE)  # timeoutFin when time out
        self.sm.signal(signal.sntcls, None)

    def timeoutFin(self):
        Protocol.connectionLost(self, reason=ConnectionDone)
        self.higherProtocol().connectionLost(ConnectionDone)
        self.higherProtocol().transport=None
        self.setHigherProtocol(None)
        self.sm.signal(signal.rcvcls, None)

    def connectionLost(self, reason=ConnectionDone):
        logger.log('info', 'connectionLost')

    def dataReceived(self, data):
        pass
