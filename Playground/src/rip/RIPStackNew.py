'''
Created on Oct 18, 2016

@author: xhyu
'''
from twisted.internet.protocol import Factory
from playground.network.common.Protocol import  StackingFactoryMixin
from RIPClientProtocolNew import RipClientProtocol
from RIPServerProtocolNew import RipServerProtocol

class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol


class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

# TODO: no sign in handshake

ConnectFactory = RipClientFactory
ListenFactory = RipServerFactory