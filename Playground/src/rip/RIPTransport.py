from playground.network.common.Protocol import StackingTransport
from RIPConfig import HANDSHAKE_STEP0, HANDSHAKE_STEP1, DATA_TRANSPORT, DEBUG, STRICT
from RIPConfig import MAX_SEQUENCE_NUMBER, MAX_NONCE_NUMBER, MAX_SEGMENT_SIZE
from RIPConfig import logger
from RIPConfig import SENDER_WINDOW_SIZE, RECEIVER_WINDOW_SIZE, TIMEOUT, TIMEOUT_HANDSHAKE, SENDER_BUFFER_TIMER_NUMBER
from RIPMessage import RipMessage
from playground.network.common.Timer import OneshotTimer, callLater
from RIPConfig import random_error_happen
from RIPConfig import ERROR_TEST

class RipTransport(StackingTransport):
    def __init__(self, lowerTransport, protocol):
        if DEBUG:
            print "rip transport init"
        StackingTransport.__init__(self, lowerTransport)
        self.__protocol = protocol
        self.__seq_num = 0
        self.__ack_num = 0
        self.__certs = []
        self.__session_id = ""
        self.__crypto = object
        self.__databuffer = ""
        self.__ripbuffer = {}
        self.__timer = []
        self.__map_seq_timer = {}
        self.__map_timer_seq = {}
        self.__unused_timer=[]
        self.__build_timers(SENDER_BUFFER_TIMER_NUMBER)

    def __window_cur_size(self):
        return self.__protocol.get_window_cur_size()

    def __window_cur_pos(self):
        return self.__protocol.get_window_cur_pos()

    def __construct_packet(self, data):
        ripMessage = RipMessage()
        seq_num = self.__protocol.get_seq_num()
        ack_flag, ack_num = self.__protocol.get_ack_num()
        ripMessage.sequence_number = seq_num
        ripMessage.acknowledgement_number = ack_num
        ripMessage.certificate = ""
        ripMessage.sessionID = self.__protocol.get_session_id()
        ripMessage.acknowledgement_flag = ack_flag
        ripMessage.close_flag = self.__protocol.get_FIN()
        ripMessage.sequence_number_notification_flag = ""
        ripMessage.reset_flag = self.__protocol.get_RST()
        ripMessage.data = data
        ripMessage.OPTIONS = ""
        ripMessage.signature = ""
        sig_data = ripMessage.__serialize__()
        sig = self.__protocol.sign(sig_data)
        ripMessage.signature = sig
        return ripMessage

    def __after_send(self, ripMessage, resend, is_client):
        logger.log('snd', ripMessage)
        # first start timer(seq_num) then inc seq_num
        if is_client:
            if self.__protocol.get_status() in [DATA_TRANSPORT, HANDSHAKE_STEP1] and ripMessage.data == "" and ripMessage.acknowledgement_flag:
                return
        else:
            if self.__protocol.get_status() in [DATA_TRANSPORT] and ripMessage.data == "" and ripMessage.acknowledgement_flag:
                return
        seq_num = ripMessage.sequence_number
        ack_num = self.__protocol.calc_expected_ack(ripMessage)
        len_d = ack_num - seq_num
        if self.__map_seq_timer.has_key(seq_num):
            return
        self.__timer_start(seq_num)
        self.__protocol.add_map_seq_expected_ack(ripMessage)
        if resend:
            return
        self.__ripbuffer[seq_num] = ripMessage
        self.__updata_window_size(len_d)
        self.__protocol.inc_seq_num(len_d)
        return

    def __build_timers(self, num):
        for i in range(num):
            self.__timer.append(OneshotTimer(self.__timeout, i))
        self.__unused_timer = range(num)

    def __get_an_unused_timer(self):
        if self.__unused_timer.__len__() == 0:
            logger.log('err', 'no free timer!!!!!!!!')
            self.__protocol.reset_connection()
        ind = self.__unused_timer.pop(0)
        return ind

    def __return_an_unused_timer(self, ind, seq):
        self.__unused_timer.append(ind)
        self.__map_seq_timer.pop(seq)
        self.__map_timer_seq.pop(ind)
        return

    def resend_not_timeout_packet(self, seq):
        if not self.__map_seq_timer.has_key(seq):
            return
        timer_ind = self.__map_seq_timer[seq]
        self.__timer[timer_ind].cancel()
        self.__return_an_unused_timer(timer_ind, seq)
        logger.log('info', 'resend not timeout packet, seq_num: ' + str(seq))
        self.__resend(seq)
        pass

    def __timeout(self, timer_ind):
        seq = self.__map_timer_seq[timer_ind]
        if not self.__map_seq_timer.has_key(seq):
            return
        self.__return_an_unused_timer(timer_ind, seq)
        logger.log('info', 'timeout seq_num: ' + str(seq))
        self.__resend(seq)

    def __timer_start(self, seq):
        if DEBUG:
            print "timer using number", SENDER_BUFFER_TIMER_NUMBER - len(self.__unused_timer)
        timer_ind = self.__get_an_unused_timer()
        self.__map_seq_timer[seq] = timer_ind
        self.__map_timer_seq[timer_ind] = seq
        if self.__protocol.get_status() not in [DATA_TRANSPORT]:
            self.__timer[timer_ind].run(TIMEOUT_HANDSHAKE)
        else:
            self.__timer[timer_ind].run(TIMEOUT)

    def __timer_stop(self, seq):
        if not self.__map_seq_timer.has_key(seq):
            return
        timer_ind = self.__map_seq_timer[seq]
        self.__timer[timer_ind].cancel()
        self.__return_an_unused_timer(timer_ind, seq)

    def __resend(self, seq):
        ripMessage = self.__ripbuffer[seq]
        print 'resend rip.seq', ripMessage.sequence_number
        self.write_ripMessage(ripMessage, True)

    def seq_that_rec_ack(self, seq):
        logger.log('info', 'seq num that received ack:'+str(seq))
        # logger.log('err', 'seq num that received ack:'+str(seq))
        if self.__ripbuffer.has_key(seq):
            logger.log('info', 'seq num that rec ack and in ripbuffer')
            # logger.log('err', 'seq num that rec ack and in ripbuffer')
            self.__ripbuffer.pop(seq)
            self.__timer_stop(seq)

    def __get_segment_data(self):
        if len(self.__databuffer) >= MAX_SEGMENT_SIZE:
            seg_data = self.__databuffer[:MAX_SEGMENT_SIZE]
            bytes_used = MAX_SEGMENT_SIZE
        else:
            seg_data = self.__databuffer
            bytes_used = len(self.__databuffer)
        return seg_data, bytes_used

    def __window_full(self, bytes_used):
        return self.__window_cur_size() + bytes_used >= SENDER_WINDOW_SIZE

    def __updata_window_size(self, value):
        self.__protocol.add_window_cur_size(value)

    def __update_buffer(self, bytes_used):
        self.__databuffer = self.__databuffer[bytes_used:]

    def write(self, data):
        self.__databuffer += data

        seg_data, bytes_used = self.__get_segment_data()
        if not self.__window_full(bytes_used):
            self.__update_buffer(bytes_used)
            ripMessage= self.__construct_packet(seg_data)
            self.write_ripMessage(ripMessage)
            self.__databuffer and callLater(0, self.write, "")

    def write_ripMessage(self, ripMessage, resend = False, is_client = False):
        # if DEBUG:
        #     print '__serialize__', ripMessage.__serialize__()
        if ERROR_TEST:
            if not random_error_happen():
                self.lowerTransport().write(ripMessage.__serialize__())
        else:
            self.lowerTransport().write(ripMessage.__serialize__())
        self.__after_send(ripMessage, resend, is_client)

