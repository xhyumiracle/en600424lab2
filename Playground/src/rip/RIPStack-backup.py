'''
Created on Oct 18, 2016

@author: xhyu
'''
import CertFactory
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING
from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin
from playground.network.message.StandardMessageSpecifiers import UINT4, OPTIONAL, STRING, DEFAULT_VALUE, LIST, BOOL1
from RIPConfig import HANDSHAKE_STEP0, HANDSHAKE_STEP1, DATA_TRANSPORT, DEBUG, STRICT
from RIPConfig import MAX_SEQUENCE_NUMBER, MAX_NONCE_NUMBER, MAX_SEGMENT_SIZE
from RIPConfig import logger
import random
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from playground.crypto import X509Certificate
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher
from RIPClientProtocol import RipClientProtocol
from RIPServerProtocol import RipServerProtocol


class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIPStack"
    MESSAGE_VERSION = "1.0"

    BODY = [
        # ("sequence_number", UINT4),
        ("sequence_number", UINT4, OPTIONAL),

        ("acknowledgement_number", UINT4, OPTIONAL),

        ("signature", STRING, DEFAULT_VALUE("")),

        ("certificate", LIST(STRING), OPTIONAL),

        # ("sessionID", STRING),
        ("sessionID", STRING, OPTIONAL),

        ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),

        ("close_flag", BOOL1, DEFAULT_VALUE(False)),

        ("sequence_number_notification_flag", BOOL1,
         DEFAULT_VALUE(False)),

        ("reset_flag", BOOL1, DEFAULT_VALUE(False)),

        ("data", STRING, DEFAULT_VALUE("")),

        ("OPTIONS", LIST(STRING), OPTIONAL)
    ]


class RipTransport(StackingTransport):
    def __init__(self, lowerTransport, protocol):
        if DEBUG:
            print "rip transport init"
        StackingTransport.__init__(self, lowerTransport)
        self.__protocol = protocol
        self.__seq_num = 0
        self.__ack_num = 0
        self.__certs = []
        self.__status = 0
        self.__session_id = ""
        self.__crypto = object
        self.__buffer = ""

    def __construct_packet(self, data):
        ripMessage = RipMessage()
        ripMessage.sequence_number = self.__protocol.get_seq_num()
        ripMessage.acknowledgement_number = self.__protocol.get_ack_num()
        ripMessage.certificate = ""
        ripMessage.sessionID = self.__protocol.get_session_id()
        ripMessage.acknowledgement_flag = self.__protocol.get_ACK()
        ripMessage.close_flag = self.__protocol.get_FIN()
        ripMessage.sequence_number_notification_flag = ""
        ripMessage.reset_flag = self.__protocol.get_RST()
        ripMessage.data = data
        ripMessage.OPTIONS = ""
        ripMessage.signature = ""
        sig_data = ripMessage.__serialize__()
        sig = self.__protocol.sign(sig_data)
        ripMessage.signature = sig
        return ripMessage

    def __after_send(self, ripMessage):
        logger.log('snd', ripMessage)
        # first start timer(seq_num) then inc seq_num
        self.__protocol.inc_seq_num()
        return

    def write(self, data):
        self.__buffer += data

        if len(self.__buffer) > MAX_SEGMENT_SIZE:
            seg_data = self.__buffer[:MAX_SEGMENT_SIZE]
            self.__buffer = self.__buffer[MAX_SEGMENT_SIZE:]
        else:
            seg_data = self.__buffer
            self.__buffer = ""

        ripMessage = self.__construct_packet(seg_data)
        self.lowerTransport().write(ripMessage.__serialize__())
        self.__after_send(ripMessage)
        self.__buffer and self.write("")

    def write_ripMessage(self, ripMessage):
        self.lowerTransport().write(ripMessage.__serialize__())
        self.__after_send(ripMessage)

class RipCrypto():
    def __init__(self, rawKey):
        self.rsa = self.importSk(rawKey)

    def importSk(self, rawKey):
        # with open(path) as f:
        #     rawKey = f.read()
        rsaKey = RSA.importKey(rawKey)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        return rsaSigner

    def sign(self, data):
        hasher = SHA256.new()
        hasher.update(data)
        signatureBytes = self.rsa.sign(hasher)
        return signatureBytes

    def loadCert(self, certBytes):
        cert = X509Certificate.loadPEM(certBytes)
        return cert

    def verifySignature(self, certBytes, data, signature):
        cert = self.loadCert(certBytes)
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(data)
        result = rsaVerifier.verify(hasher, signature)
        return result

    def getCommonName(self, certBytes):
        cert = self.loadCert(certBytes)
        return cert.getSubject()["commonName"]

    def verifyCertChain(self, rootCertBytes, certBytes):
        # TODO: verify the intermediate cert by root's public key
        cert = self.loadCert(certBytes)
        rootCert = self.loadCert(rootCertBytes)
        if (cert.getIssuer() != rootCert.getSubject()):
            return False
        # now let's check the signature
        rootPkBytes = rootCert.getPublicKeyBlob()
        # use rootPkBytes to get a verifiying RSA key
        # not shown here. Use the code above
        rootPk = RSA.importKey(rootPkBytes)
        rootVerifier = PKCS1_v1_5.new(rootPk)

        bytesToVerify = cert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rootVerifier.verify(hasher, cert.getSignatureBlob()):
            return False
        return True

    def getPeerPkEncrypter(self, certBytes):
        cert = self.loadCert(certBytes)
        peerPublicKeyBytes = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBytes)
        peerRsaEncrypter = PKCS1OAEP_Cipher(peerPublicKey, None, None, None)
        return peerRsaEncrypter

    def getPeerPkDecrypter(self, keyBytes):
        key = RSA.importKey(keyBytes)
        rsaDecrypter = PKCS1OAEP_Cipher(key, None, None, None)
        return rsaDecrypter


class RipClientProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        if DEBUG:
            print "rip protocol init"
            print "logprefix", self.logPrefix()
        self.__buffer = ""
        self.__status=HANDSHAKE_STEP0
        self.get_config()
        self.__crypto = RipCrypto(self.__mySkBytes)
        self.__ack_num = 0
        self.__ACK = False
        self.__FIN = False
        self.__RST = False
        self.__seq_num = 0
        self.__session_id = ""
        self.__peer_session_id = ""
        self.__Nonce1 = 0
        self.__Nonce2 = 0

    def get_seq_num(self):
        return self.__seq_num
    def get_ack_num(self):
        return self.__ack_num
    def get_session_id(self):
        return self.__session_id
    def get_ACK(self):
        return self.__ACK
    def get_FIN(self):
        return self.__FIN
    def get_RST(self):
        return self.__RST
    def sign(self, data):
        return self.__crypto.sign(data)

    def inc_seq_num(self):
        self.__seq_num += 1

    def get_config(self):
        # self.__common_name_prefix = "20164.1"
        # self.__common_name_group = "47806"
        # self.__common_name_my = "1"
        # self.__get_my_cert()
        # self.__get_CA_cert()
        # self.__get_sk()
        # self.__certs = [self.__myCertBytes, self.__CACertBytes]
        self.__mySkBytes = CertFactory.getPrivateKeyForAddr("20164.1.100.1")
        self.__certs = CertFactory.getCertsForAddr("20164.1.100.1")

    def __get_my_cert(self):
        with open(self.my_cert_path) as f:
            certBytes = f.read()
        self.__myCertBytes = certBytes
        # return certBytes, ["123", "234"]

    def __get_CA_cert(self):
        with open(self.CA_cert_path) as f:
            CACertBytes = f.read()
        self.__CACertBytes = CACertBytes

    def __get_sk(self):
        with open(self.my_sk_path) as f:
            skBytes = f.read()
        self.__mySkBytes = skBytes

    def __write(self, data="", is_customize=False,  seq_num=0, ack_num=0, certs=[], session_id=0, ACK=False, FIN=False, SNN=False, RST=False, options=[]):
        if is_customize:
            ripMessage = RipMessage()
            ripMessage.sequence_number = seq_num
            ripMessage.acknowledgement_number = ack_num
            ripMessage.certificate = certs
            ripMessage.sessionID = session_id
            ripMessage.acknowledgement_flag = ACK
            ripMessage.close_flag = FIN
            ripMessage.sequence_number_notification_flag = SNN
            ripMessage.reset_flag = RST
            ripMessage.OPTIONS = options
            ripMessage.signature = ""
            sig_data = ripMessage.__serialize__()
            sig = self.__sign(sig_data)
            ripMessage.signature = sig
            self.__higherTransport.write_ripMessage(ripMessage)
        else:
            self.__higherTransport.write(data)

    def __verify_certs(self, ripMessage, ind_cert):
        # ripMessage.certificate
        certs = ripMessage.certificate
        if not self.__crypto.verifyCertChain(certs[ind_cert + 1], certs[ind_cert + 0]):
            logger.log('err', 'cert chain verify failed')
            return False
        return True

    def __verify_common_name(self, ripMessage, ind_cert):
        certs = ripMessage.certificate
        if DEBUG:
            logger.log('info', "common name form cert: "+self.__crypto.getCommonName(certs[ind_cert + 0]))
        return True

    def __set_ack_flag(self, ripMessage):
        if not ripMessage.acknowledgement_flag:
            logger.log('info', 'ACK flag is False')
            return False
        logger.log('info', 'ACK flag is True')
        return True

    def __verify_ack_num(self, ripMessage):
        if self.__seq_num != ripMessage.acknowledgement_number:
            logger.log('err', 'verify ack num failed')
            return False
        return True

    def __verify_sessionID(self, ripMessage):
        if ripMessage.sessionID != self.__peer_session_id:
            logger.log('err', 'verify sessionID failed')
        return True

    def __verify_seq_num(self, ripMessage):
        if ripMessage.sequence_number != self.__ack_num:
            logger.log('err', 'seq number wrong, seq num: ' + ripMessage.sequence_number + ' ack num' + self.__ack_num)
            return False
        return True

    def __set_SNN(self, ripMessage):
        if not ripMessage.sequence_number_notification_flag:
            logger.log('info', 'SNN is False')
            return False
        return True

    def __verify_signature(self, ripMessage):
        certBytes = self.__peer_cert
        signature = ripMessage.signature
        ripMessage.signature = ""
        data = ripMessage.__serialize__()
        if not self.__crypto.verifySignature(certBytes, data, signature):
            logger.log('err', 'signature verify failed')
            return False
        return True

    def __sign(self, data):
        return self.__crypto.sign(data)

    def __verify_Nonce1(self, ripMessage):
        signed_nonce1 = ripMessage.certificate[1]
        certBytes = self.__peer_cert
        # TODO: Nonce signed with str or int?
        if not self.__crypto.verifySignature(certBytes, hex(self.__Nonce1+1)[2:], signed_nonce1):
            logger.log('err', 'verify Nonce1 failed')
            return False
        return True

    def __send_1st_packet(self):
        logger.log('hs', 'step1')
        self.__seq_num = random.randint(0, MAX_SEQUENCE_NUMBER)
        self.__Nonce1 = random.randint(0, MAX_NONCE_NUMBER)
        certs = [hex(self.__Nonce1)[2:]] + self.__certs
        # TODO: does the server sequence number need to be the ACK number of client?
        self.__write("", True, self.__seq_num, 0, certs, 0, False, False, True, False, [])

    def __check_2nd_packet(self, ripMessage):
        num_nonce = 2
        logger.log('hs', 'step2')
        if not self.__verify_certs(ripMessage, num_nonce):
            return False
        if not self.__verify_common_name(ripMessage, num_nonce):
            return False
        self.__peer_cert = ripMessage.certificate[num_nonce + 0]
        if not self.__verify_signature(ripMessage):
            return False
        if not self.__set_ack_flag(ripMessage):
            return False
        if not self.__verify_ack_num(ripMessage):
            return False
        if not self.__set_SNN(ripMessage):
            return False
        if not self.__verify_Nonce1(ripMessage):
            return False
        self.__handle_event('rec_SEQ', ripMessage)
        self.__Nonce2 = int(ripMessage.certificate[0], 16)
        return True

    def __send_3rd_packet(self):
        logger.log('hs', 'step3')
        signed_nonce2 = self.__sign(hex(self.__Nonce2 + 1)[2:])
        certs = [signed_nonce2] + self.__certs
        self.__write("", True, self.__seq_num, self.__ack_num, certs, 0, True, False, False, False, [])

    def __handshake(self, ripMessage):
        if self.__status == HANDSHAKE_STEP0:
            self.__send_1st_packet()
            self.__status = HANDSHAKE_STEP1
        elif self.__status == HANDSHAKE_STEP1:
            success = self.__check_2nd_packet(ripMessage)
            if not success:
                logger.log('err', "check 2nd failed, data:\n" + str(ripMessage))
                # TODO: how to handle this when handshake failed?
                self.__status = HANDSHAKE_STEP0
                return False
            self.__send_3rd_packet()
            self.__status = DATA_TRANSPORT
            self.__handle_event('handshake_finish', object)
            # handshake success
            self.makeHigherConnection(self.__higherTransport)
        return True

    def __set_reset_flag(self, ripMessage):
        return ripMessage.reset_flag

    def __set_close_flag(self, ripMessage):
        return ripMessage.close_flag

    def __handle_event(self, event, ripMessage):
        if event == 'rec_RST':
            pass
        elif event == 'rec_FIN':
            pass
        elif event == 'rec_ACK':
            # stop timer(ack-1)
            self.__ACK = False
            pass
        elif event == 'rec_SEQ':
            self.__ack_num = ripMessage.sequence_number + 1
            self.__ACK = True
        elif event == 'rec_rip':
            if not self.__verify_signature(ripMessage):
                return False
            if not self.__verify_sessionID(ripMessage):
                return False
            if self.__set_ack_flag(ripMessage):
                if not self.__verify_ack_num(ripMessage):
                    return False
            if self.__set_reset_flag(ripMessage):
                self.__handle_event('rec_RST', ripMessage)
                return False
            if self.__set_close_flag(ripMessage):
                self.__handle_event('rec_FIN', ripMessage)
            self.__handle_event('rec_ACK', ripMessage)
            self.__handle_event('rec_SEQ', ripMessage)
            self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        elif event == 'handshake_finish':
            self.__session_id = hex(self.__Nonce1)[2:] + hex(self.__Nonce2)[2:]
            self.__peer_session_id = hex(self.__Nonce2)[2:] + hex(self.__Nonce1)[2:]
            print 'nonce1:', hex(self.__Nonce1)
            print 'nonce2:', hex(self.__Nonce2)
            print 'session:', self.__session_id
        else:
            logger.log('err', 'wrong event, seems you got a bug man, event: ' + event)
        return True

    def connectionMade(self):
        logger.log('con')
        self.__higherTransport = RipTransport(self.transport, self)
        self.__handshake("")

    def dataReceived(self, data):
        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(data)
            self.__buffer = self.__buffer[bytesUsed:]
        except Exception, e:
            # print "We had a deserialization error", e
            return

        logger.log('rcv', ripMessage)
        print 'seq nun', self.__seq_num
        if self.__status != DATA_TRANSPORT:
            success = self.__handshake(ripMessage)
            if not success:
                # TODO: lose connection
                exit(0)
        else:
            if not self.__handle_event('rec_rip', ripMessage):
                return
        self.__buffer and self.dataReceived("")

    def __sendMessage(self, data):
        logger.log('snd', data)
        self.__higherTransport.write(data)

    def __beautify_print_rip(self, ripMessage):
        ripstr = ""
        ripstr += '>ACK flag:' + str(ripMessage.acknowledgement_flag) + '\n'
        ripstr += '>ACK num:' + str(ripMessage.acknowledgement_number) + '\n'
        ripstr += '>SEQ num:' + str(ripMessage.sequence_number) + '\n'
        ripstr += '>data:' + ripMessage.data + '\n'
        return ripstr

    def __log(self, type, data = ''):
        ripstr = '{rip}'
        logstr = '{rip}Null log, how can this be ??? You did write out a bug dude!'
        if type == 'con':
            logstr = ripstr + '{CON}'
        elif type == 'rcv':
            logstr = ripstr + '{RCV}' + '\n' + self.__beautify_print_rip(data)
        elif type == 'snd':
            logstr = ripstr + '{SND}' + '\n' + self.__beautify_print_rip(data)
        elif type == 'err':
            logstr = ripstr + '{ERROR}' + data
        elif type == 'hs':
            logstr = ripstr + '{Handshake}' + data
        elif type == 'info':
            logstr = ripstr + '{info}' + data
        print logstr


class RipServerProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        if DEBUG:
            print "rip protocol init"
            print "logprefix", self.logPrefix()
        self.__buffer = ""
        self.__status=HANDSHAKE_STEP0
        self.get_config()
        self.__crypto = RipCrypto(self.__mySkBytes)
        self.__ack_num = 0
        self.__ACK = False
        self.__FIN = False
        self.__RST = False
        self.__session_id = ""
        self.__peer_session_id = ""
        self.__Nonce1 = 0
        self.__Nonce2 = 0

    def get_seq_num(self):
        return self.__seq_num
    def get_ack_num(self):
        return self.__ack_num
    def get_session_id(self):
        return self.__session_id
    def get_ACK(self):
        return self.__ACK
    def get_FIN(self):
        return self.__FIN
    def get_RST(self):
        return self.__RST
    def sign(self, data):
        return self.__crypto.sign(data)

    def inc_seq_num(self):
        self.__seq_num += 1

    def get_config(self):
        # self.__common_name_prefix = "20164.1"
        # self.__common_name_group = "47806"
        # self.__common_name_my = "1"
        # self.my_sk_path = "/home/xhyu/.ssh/keyforNetSec/20164.1.47806.1/20164.1.47806.1.rsa"
        # self.my_cert_path = "/home/xhyu/.ssh/keyforNetSec/20164.1.47806.1/20164.1.47806.1.cert"
        # self.CA_cert_path = "/home/xhyu/.ssh/keyforNetSec/xhyu_rsa_signed.cert"
        # self.__get_my_cert()
        # self.__get_CA_cert()
        # self.__get_sk()
        # self.__certs = [self.__myCertBytes, self.__CACertBytes]
        self.__mySkBytes = CertFactory.getPrivateKeyForAddr("20164.1.100.1")
        self.__certs = CertFactory.getCertsForAddr("20164.1.100.1")

    def __get_my_cert(self):
        with open(self.my_cert_path) as f:
            certBytes = f.read()
        self.__myCertBytes = certBytes
        # return certBytes, ["123", "234"]

    def __get_CA_cert(self):
        with open(self.CA_cert_path) as f:
            CACertBytes = f.read()
        self.__CACertBytes = CACertBytes

    def __get_sk(self):
        with open(self.my_sk_path) as f:
            skBytes = f.read()
        self.__mySkBytes = skBytes

    def connectionMade(self):
        logger.log('con')
        self.__higherTransport = RipTransport(self.transport, self)

    def __write(self, data="", is_customize=False,  seq_num=0, ack_num=0, certs=[], session_id=0, ACK=False, FIN=False, SNN=False, RST=False, options=[]):
        if is_customize:
            ripMessage = RipMessage()
            ripMessage.sequence_number = seq_num
            ripMessage.acknowledgement_number = ack_num
            ripMessage.certificate = certs
            ripMessage.sessionID = session_id
            ripMessage.acknowledgement_flag = ACK
            ripMessage.close_flag = FIN
            ripMessage.sequence_number_notification_flag = SNN
            ripMessage.reset_flag = RST
            ripMessage.OPTIONS = options
            ripMessage.signature = ""
            sig_data = ripMessage.__serialize__()
            sig = self.__sign(sig_data)
            ripMessage.signature = sig
            logger.log('snd', ripMessage)
            self.__higherTransport.write_ripMessage(ripMessage)
        else:
            self.__higherTransport.write(data)

    def __verify_certs(self, ripMessage, ind_cert):
        # TODO: verify the intermediate cert by root's public key
        print 'verifying certs'
        # ripMessage.certificate
        certs = ripMessage.certificate
        if not self.__crypto.verifyCertChain(certs[ind_cert + 1], certs[ind_cert + 0]):
            logger.log('err', 'cert chain verify failed')
            return False
        return True

    def __verify_common_name(self, ripMessage, ind_cert):
        certs = ripMessage.certificate
        if DEBUG:
            logger.log('info', "common name form cert: "+self.__crypto.getCommonName(certs[ind_cert + 0]))
        return True

    def __verify_signature(self, ripMessage):
        certBytes = self.__peer_cert
        signature = ripMessage.signature
        ripMessage.signature = ""
        data = ripMessage.__serialize__()
        if not self.__crypto.verifySignature(certBytes, data, signature):
            logger.log('err', 'signature verify failed')
            return False
        return True

    def __set_ack_flag(self, ripMessage):
        if not ripMessage.acknowledgement_flag:
            logger.log('info', 'ACK flag is False')
            return False
        logger.log('info', 'ACK flag is True')
        return True

    def __verify_ack_num(self, ripMessage):
        if self.__seq_num != ripMessage.acknowledgement_number:
            logger.log('err', 'verify ack num failed')
            return False
        return True

    def __set_SNN(self, ripMessage):
        if not ripMessage.sequence_number_notification_flag:
            logger.log('info', 'SNN is False')
            return False
        return True

    def __verify_Nonce2(self, ripMessage):
        signed_nonce2 = ripMessage.certificate[0]
        certBytes = self.__peer_cert
        if not self.__crypto.verifySignature(certBytes, hex(self.__Nonce2+1)[2:], signed_nonce2):
            logger.log('err', 'verify Nonce2 failed')
            return False
        return True

    def __sign(self, data):
        return self.__crypto.sign(data)

    def __check_1st_packet(self, ripMessage):
        num_nonce = 1
        logger.log('hs', 'step1')
        if not self.__verify_certs(ripMessage, num_nonce):
            return False
        if not self.__verify_common_name(ripMessage, num_nonce):
            return False
        self.__peer_cert = ripMessage.certificate[num_nonce + 0]
        if not self.__verify_signature(ripMessage):
            return False
        if STRICT and not self.__set_SNN(ripMessage):
            return False
        self.__handle_event('rec_SEQ', ripMessage)
        self.__Nonce1 = int(ripMessage.certificate[0], 16)
        return True

    def __send_2nd_packet(self):
        logger.log('hs', 'step2')
        self.__seq_num = random.randint(0, MAX_SEQUENCE_NUMBER)
        self.__Nonce2 = random.randint(0, MAX_NONCE_NUMBER)
        signed_nonce1 = self.__sign(hex(self.__Nonce1 + 1)[2:])
        certs = [hex(self.__Nonce2)[2:], signed_nonce1] + self.__certs
        self.__write("", True, self.__seq_num, self.__ack_num, certs, 0, True, False, True, False, [])

    def __check_3rd_packet(self, ripMessage):
        logger.log('hs', 'step3')
        if not self.__verify_signature(ripMessage):
            return False
        if not self.__set_ack_flag(ripMessage):
            return False
        if not self.__verify_ack_num(ripMessage):
            return False
        if not self.__verify_Nonce2(ripMessage):
            return False
        if not self.__verify_seq_num(ripMessage):
            return False
        self.__ack_num = ripMessage.sequence_number + 1
        return True

    def __handshake(self, ripMessage=""):
        print "shake", self.__status
        if self.__status == HANDSHAKE_STEP0:
            success = self.__check_1st_packet(ripMessage)
            if not success:
                logger.log('err', "check 1st failed, data:" + str(ripMessage))
                # TODO: how to handle failure?
                self.__status = HANDSHAKE_STEP0
                return False
            self.__send_2nd_packet()
            self.__status = HANDSHAKE_STEP1
        elif self.__status == HANDSHAKE_STEP1:
            success = self.__check_3rd_packet(ripMessage)
            if not success:
                logger.log('err', "check 3rd failed, data:" + str(ripMessage))
                return False
            self.__status = DATA_TRANSPORT
            self.__handle_event('handshake_finish', object)
            # handshake success
            self.makeHigherConnection(self.__higherTransport)
        return True

    def __verify_sessionID(self, ripMessage):
        if ripMessage.sessionID != self.__peer_session_id:
            logger.log('err', 'verify sessionID failed')
        return True

    def __verify_seq_num(self, ripMessage):
        if ripMessage.sequence_number != self.__ack_num:
            logger.log('err', 'seq number wrong, seq num: ' + ripMessage.sequence_number + ' ack num' + self.__ack_num)
            return False
        return True

    def __set_reset_flag(self, ripMessage):
        return ripMessage.reset_flag

    def __set_close_flag(self, ripMessage):
        return ripMessage.close_flag

    def __handle_event(self, event, ripMessage):
        if event == 'rec_RST':
            pass
        elif event == 'rec_FIN':
            pass
        elif event == 'rec_ACK':
            # stop timer(ack-1)
            self.__ACK = False
        elif event == 'rec_SEQ':
            self.__ack_num = ripMessage.sequence_number + 1
            self.__ACK = True
        elif event == 'rec_rip':
            if not self.__verify_signature(ripMessage):
                return False
            if not self.__verify_sessionID(ripMessage):
                return False
            if self.__set_ack_flag(ripMessage):
                if not self.__verify_ack_num(ripMessage):
                    return False
            if self.__set_reset_flag(ripMessage):
                self.__handle_event('rec_RST', ripMessage)
                return False
            if self.__set_close_flag(ripMessage):
                self.__handle_event('rec_FIN', ripMessage)
            self.__handle_event('rec_ACK', ripMessage)
            self.__handle_event('rec_SEQ', ripMessage)
            self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        elif event == 'handshake_finish':
            self.__session_id = hex(self.__Nonce2)[2:] + hex(self.__Nonce1)[2:]
            self.__peer_session_id = hex(self.__Nonce1)[2:] + hex(self.__Nonce2)[2:]
            print 'nonce1:', hex(self.__Nonce1)
            print 'nonce2:', hex(self.__Nonce2)
            print 'session:', self.__session_id
        else:
            logger.log('err', 'wrong event, seems you got a bug man, event: ' + event)
        return True

    def dataReceived(self, data):
        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(data)
            self.__buffer = self.__buffer[bytesUsed:]
        except Exception, e:
            # print "We had a deserialization error", e
            return

        logger.log('rcv', ripMessage)
        print 'peer session id', self.__peer_session_id
        if self.__status != DATA_TRANSPORT:
            success = self.__handshake(ripMessage)
            if not success:
                # TODO: lose connection
                print "here!!!!!!!!!!"
                self.__status = HANDSHAKE_STEP0
                return
        else:
            if not self.__handle_event('rec_rip', ripMessage):
                return
        self.__buffer and self.dataReceived("")

    def __sendMessage(self, data):
        self.__higherTransport.write(data)

    def __beautify_print_rip(self, ripMessage):
        ripstr = ""
        ripstr += '>ACK flag:' + str(ripMessage.acknowledgement_flag) + '\n'
        ripstr += '>ACK num:' + str(ripMessage.acknowledgement_number) + '\n'
        ripstr += '>SEQ num:' + str(ripMessage.sequence_number) + '\n'
        ripstr += '>data:' + ripMessage.data + '\n'
        return ripstr

    def __log(self, type, data = ''):
        ripstr = '{rip}'
        logstr = '{rip}Null log, how can this be ??? You did write out a bug dude!'
        if type == 'con':
            logstr = ripstr + '{CON}'
        elif type == 'rcv':
            logstr = ripstr + '{RCV}' + '\n' + self.__beautify_print_rip(data)
        elif type == 'snd':
            logstr = ripstr + '{SND}' + '\n' + self.__beautify_print_rip(data)
        elif type == 'err':
            logstr = ripstr + '{ERROR}' + data
        elif type == 'hs':
            logstr = ripstr + '{Handshake}' + data
        elif type == 'info':
            logstr = ripstr + '{info}' + data
        print logstr


class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol


class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

# TODO: no sign in handshake

ConnectFactory = RipClientFactory
ListenFactory = RipServerFactory