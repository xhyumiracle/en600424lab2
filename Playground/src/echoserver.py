import re
import lab2stack

from twisted.internet import reactor
from twisted.internet.protocol import Protocol, Factory

# from rip import RIPStackNew
from playground.twisted.endpoints import GateServerEndpoint

global client
port = 8124

class EchoServer(Protocol):
    def connectionMade(self):
        global client
        clientip = self.transport.getPeer().host
        clientport = self.transport.getPeer().port
        client = str(clientip) + ':' + str(clientport)
        self.log(client, 'con')

    def dataReceived(self, data):
        global client
        self.log(client, 'rcv', data)
        self.sendMessage(data)

    def sendMessage(self, data):
        global client
        self.transport.write(data)
        self.log(client, 'snd', data)

    def log(self, client, type, data = ''):
        clientaddr = '[' + client + ']'
        if type == 'con':
            logstr = '[CON]' + clientaddr + 'has connected'
        elif type == 'rcv':
            logstr = '[RCV]' + clientaddr + '\n' + data
        elif type == 'snd':
            logstr = '[SND]' + clientaddr + '\n' + data
        elif type == 'req':
            logstr = '[REQ]' + clientaddr + data
        # print logstr


class EchoFactory(Factory):
    def buildProtocol(self, addr):
        return EchoServer()

#endpoint = TCP4ServerEndpoint(reactor, port)
#endpoint = GateServerEndpoint.CreateFromConfig(reactor, 101,"xhyumiracle")
endpoint = GateServerEndpoint(reactor, port, "127.0.0.1", 19090, networkStack=lab2stack)
endpoint.listen(EchoFactory())
reactor.run()
